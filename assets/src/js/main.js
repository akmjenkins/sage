//load all required scripts
requirejs(
	[
		'scripts/components/placeholder',
		'scripts/components/anchors.external.popup',
		'scripts/components/standard.accordion',
		'scripts/components/gmap',
		'scripts/components/custom.select',
		'scripts/swipe.srf',
		
		'scripts/device.pixel.ratio',
		
		'scripts/blocks',
		'scripts/locations',
		'scripts/hero',
		'scripts/nav',
		'scripts/help'
	],
	function() {		
	
		$('#view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});
	
	}
);