(function() {

		//selector wrapper
		$('div.selector').each(function() {
			var 
				el = $(this),
				select = $('select',el),
				val = $('span.value',el);
				
			select
				.on('change',function(e) { val.html($(this).children('option:selected').text()); })
				.trigger('change');
		});
		
}());