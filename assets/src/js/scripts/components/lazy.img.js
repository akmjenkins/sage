define([],function() {
	
	var 
		throttle,
		throttleDelay = 100,
		loadImages = function() {
		
			$('img.lazy').each(function() {
				var el = $(this);
				
				if(el.css('display').toLowerCase() === 'none') { return; }
				el.attr('src',el.data('src')).addClass('lazy-loaded').removeClass('lazy');
			});
		
		};
		
	$(window)
		.on('resize',function() {
			clearTimeout(throttle);
			throttle = setTimeout(loadImages,throttleDelay);
		});
		
	loadImages();
	
	
	//no public API
	return {};

});