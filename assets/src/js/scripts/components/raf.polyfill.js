define([
],function() {
	
	return {
		requestAnimFrame: function(callback){
			var raf = (function(){
				  return  window.requestAnimationFrame       ||
						  window.webkitRequestAnimationFrame ||
						  window.mozRequestAnimationFrame    ||
						  function( callback ){
							window.setTimeout(callback, 1000 / 60);
						  };
				})();
				
			raf.apply(window,[callback]);
		},
		
		cancelAnimFrame: function(identifier){
			var caf = (function(){
				  return  window.cancelAnimationFrame       ||
						  window.webkitCancelAnimationFrame ||
						  window.mozCancelAnimationFrame    ||
						  function( idenfitier ){
							clearTimeout(identifier);
						  };
				})();
				
			caf.apply(window,[callback]);
		}
		
	}

});