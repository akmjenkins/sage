define([
],function() {


	var 
		isIE8 = $('html').hasClass('ie8'),
		$body = $('body'),
		grid,
		getGrids = function() {
			return $('div.grid-layout');
		},
		grids = getGrids(),
		throttle,
		throttleDelay = 100,
		doLayout = function() {
			grids.each(function() { grid.layout.apply(this); });
		};
		
		
	//if there are no grids on this page, then we don't need to do anything
	if(!grids.length) { return; }
		
	grid = {
		
		scrollTimeoutId: null,
		scrollTimeoutDelay: 500,
		
		onLoad: function() {
		
			//get the grids on the page
			grids = grids || getGrids();
		
			//continually check the scroll position - do not tie checking to scroll event....never tie anything directly to scroll event
			//do not even attempt to do this in IE8 - just load everything up right at the beginning
			if(!isIE8) {
				grid.checkScrollPosition();
			} else {
				grids
					.children('div.box')
					.each(function() {
						var el = $(this);
						el.addClass('visible').addClass('box-loaded') && grid.loadImageInBox(el);
					});
			}
			
			doLayout();
		},
		
		layout: function() {
			var 
				offset = 0,
				el = $(this),
				elements = el.children('div.box');
			
			//calculate the height of the grid based on the child elements
			elements.each(function(index,box) {
				var totalOffset = box.offsetTop+box.offsetHeight;				
				if(totalOffset > offset) {
					offset = totalOffset;
				}
			});
			
			el.css('height',offset+'px');
			
		},
		
		loadImageInBox: function($box) {
			$box.find('div.img').each(function() {
				var
					el = $(this),
					source = el.data('src');
					
					$('<img/>')
						.on('load',function() {
							$box.addClass('box-loaded');
							el.css('backgroundImage','url('+source+')');
						})
						.attr('src',source);
					
			});		
		},
		
		checkScrollPosition: function() {
			grid.scrollTimeoutId = setTimeout(grid.checkScrollPosition,grid.scrollTimeoutDelay);
			
			var 
				doc = document.documentElement, body = document.body,
				height = (window.innerHeight || doc && doc.clientHeight  ||  0),
				scrollOffset = (doc && doc.scrollTop  || body && body.scrollTop  || 0);
			
			grids
				.children('div.box')
				.filter(function() { return !$(this).hasClass('visible') })
				.each(function(index,box) {
					var 
						$box = $(box),
						$tab = $box.closest('div.tab');
						
						//not visible
						if($tab.length && !$tab.hasClass('selected')) { return true; } 
					
					//if height is 0, then something is wrong, so we'll just load it anyway
					( (height === 0) || ($box.position().top < scrollOffset+height/2 ) ) && $box.addClass('visible') && grid.loadImageInBox($box);
				}).length || 
				//if all boxes are visible, cancel the running timeout function, it's job is now done
				clearTimeout(grid.scrollTimeoutId);
		},
		
		showShareInBox: function($box) {
			$box.find('div.content').addClass('show-share');
		},
		
		hideShareInBox: function($box) {
			$box.find('div.content').removeClass('show-share');
		}
		
	}

	//likewise, don't bother re-doing the layout on resize in IE8, we'll probably just crash the browser
	!isIE8 && $(window)
					.on('resize',function() {
						clearTimeout(throttle);
						throttle = setTimeout(doLayout,throttleDelay);
					});
		
		
	//listen for share button
	$('div.grid-layout')
		.on('click','div.box button.share-white',function(e) {
			grid.showShareInBox($(this).closest('div.box'));
		})
		.on('mouseleave','div.box',function(e) {
			Modernizr.csstransitions || grid.hideShareInBox($(this));
		})
		.on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd','div.box',function(e) {
			var 
				el = $(this),
				t = $(e.target);
				( t.hasClass('back') || t.hasClass('front') ) && grid.hideShareInBox($(this));
			
		})
		.on('click','div.social-white button.close',function() {
			grid.hideShareInBox($(this).closest('div.box'));
		});
		
		
	//start performing the necessary operations
	grid.onLoad();
		
	
	return {
		
		//call this method to reset the classes of the boxes on the page
		reset: function() {
			grids.children('div.box').removeClass('box-loaded visible');
		},
		
		//call this method after the boxes and/or grids on the page have been replaced
		refresh: function() {
			grids = null;
			grid.onLoad();
		}
		
	};

});