define([
],function() {
	
	var 
		$locations = $('div.locations'),
		$locationsButtons = $locations.find('div.buttons button'),
		$locationsSwiperEl = $locations.find('div.swipe'),
		locationsSwiper = $locationsSwiperEl.data('swipe');
		
		$locationsButtons
			.eq(0)
			.addClass('selected')
			.end()
			.on('click',function(e) {
				locationsSwiper.slide($(this).index());
			});
		
		$locationsSwiperEl.on('swipeChanged',function(e,i,div) {
			$locationsButtons.removeClass('selected').eq(i).addClass('selected');
		});
	
});