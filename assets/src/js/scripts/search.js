define([
	'scripts/components/raf.polyfill'
],function(raf) {

	var 
		query = '',
		req,
		$html = $('html'),
		$overlay = $('div.search-overlay'),
		$results = $overlay.find('div.search-results'),
		$form = $overlay.find('form'),
		$input = $form.children('input'),
		url = $form.attr('action'), 
		url = templateJS.templateURL+'templates/inc/i-search-results.php', //this line for testing only - please remove
		showclass = 'show-search',
		loadingclass = 'is-loading',
		loadingmoreclass = 'is-loading-more',
		noresultsclass = 'no-results',
		scrollInterval = null,
		$resultsList = null,
		$contextualResult = null,
		$resultsListTabs = null,
		$resultsListTop = 0,
		methods = {
	
			updateTabPos: function() {
				var self = this;
				if(!$resultsList) {
					$resultsList = $overlay.find('div.results-list');
					$resultsListTabs = $resultsList.children('div.tab-controls');
					$contextualResult = $overlay.find('div.contextual');
				}
			
				raf.requestAnimFrame(function() {
					$resultsListTop = $resultsList.position().top;
					var top = ($resultsListTop*-1);
					top = (top < 0) ? 0 : top;
					
					if(Modernizr.csstransforms3d) {
						//better on rendering performance
						$resultsListTabs.css({
							'-webkit-transform': 'translate3d(0,'+top+'px,0)',
							'-moz-transform': 'translate3d(0,'+top+'px,0)',
							'-ms-transform': 'translate3d(0,'+top+'px,0)',
							'transform': 'translate3d(0,'+top+'px,0)',
						});
					} else {
						$resultsListTabs.css('top',top+'px');
					}

					self.updateTabPos();
				});
			
			},
			
			scrollResultsToTop: function() {
				if($resultsListTop < 0) {
					$results.scrollTop(($contextualResult) ? $contextualResult.outerHeight() : 0);
				}
			},
	
			finishedSearch: function(r,status,jqXHR) {
				if(status === 'success' && jqXHR && r) {
					$results.html(r);
					$overlay.removeClass(noresultsclass);
					this.updateTabPos();
					return;
				}
				
				this.failedSearch(arguments);
				
			},
			
			failedSearch: function(jqXHR,status,error) {
				if(status !== 'abort') {
					alert('No results found for '+query);
				}
			},
			
			completedRequest: function() {
				this.hideLoading();
				$input.blur();
			},
			
			
			loadMoreResults: function(url) {
				var self = this;
					
				$overlay.addClass(loadingmoreclass);
			
				req && req.abort();
				req = $.ajax({
					url:url
				})
					.done(function() {
						self.finishedLoadMore.apply(self,arguments);
					})
					.fail(function() {
						self.failedLoadMore.apply(self,arguments);
					})
					.always(function() {
						self.completedLoadMore.apply(self,arguments);
					});		

				return req.promise();
					
			},
			
			finishedLoadMore: function(r,status,jqXHR) {
				if(status === 'success' && jqXHR && r) {
					$results.find('div.results div.tab.selected').html(r);
					this.scrollResultsToTop();
					return;
				}
			},
			
			failedLoadMore: function(r,status,jqXHR) {
				if(status !== 'abort') {
					alert('Unable to load results');
				}
			},
			
			completedLoadMore: function() {
				$overlay.removeClass(loadingmoreclass);
			},
			
			showLoading: function() {
				$overlay.addClass(loadingclass);
			},
			
			hideLoading: function() {
				$overlay.removeClass(loadingclass);
			},
		
			doSearch: function(q) {
				var self = this;
				
				query = q;
				
				self.showLoading();
				
				req && req.abort();				
				req = $.ajax({
					url:url+'?q='+q
				})
					.done(function() {
						self.finishedSearch.apply(self,arguments);
					})
					.fail(function() {
						self.failedSearch.apply(self,arguments);
					})
					.always(function() {
						self.completedRequest.apply(self,arguments);
					});
					
				return req.promise();
			
			},
		
			showForm: function() {
				this.isVisible() || $overlay.addClass(noresultsclass);
				$html.addClass(showclass);
			},
			
			hideForm: function() {
				$html.removeClass(showclass);
				clearTimeout(scrollInterval);
			},
			
			isVisible: function() {
				return $html.hasClass(showclass);
			},
			
			toggleForm: function() {
				if(!this.isVisible()) {
					return this.showForm();
				}
				
				return this.hideForm();
			}
	
		};
	
	$(document)
		.on('click','.toggle-search-form',function(e) {
			e.preventDefault();
			methods.toggleForm();
		})
		.on('keydown',function(e) {
			e.keyCode === 27 && methods.hideForm();
		});
		
	$overlay
		.on('click','.tab-control',function(e) {
			methods.scrollResultsToTop();
		})
		.on('click','div.results div.links a',function(e) {
			methods.loadMoreResults(this.href);
			return false;
		});
	
	$form.on('submit',function(e) {
		methods.doSearch($input.val());
		return false;
	});


	return methods;
});