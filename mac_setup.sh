#!/bin/sh

#install homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#install graphicsmagick
echo "Installing GraphicsMagick...";
brew install graphicsmagick -v

#install imagemagick (for favicon grunt task)
echo "Installing ImageMagick...";
brew install imagemagick -v

#install node
echo "Installing NodeJS...";
brew install node -v

#install grunt-cli globally
echo "Installing Grunt-CLI...";
npm install grunt-cli -g

#install bower globally
echo "Installing Bower...";
npm install bower -g

#cd into assets directory and run mac_init.sh
cd assets/
sh mac_init.sh
