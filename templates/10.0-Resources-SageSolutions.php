<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Resources</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
				<article>
					
					<div class="header">
						<div class="sw">
							<h1>Resources (h1)</h1>
							<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
						</div><!-- .sw -->
					</div><!-- .header -->
					
					<div class="sw cf">
						<div class="main-body with-sidebar">
							<div class="article-body">
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. Donec eget eleifend justo. 
								Nullam vel dui elit. Nam molestie vestibulum sollicitudin. In quis ex pellentesque, feugiat dolor eu, tincidunt sapien. 
								Aliquam viverra venenatis augue at vestibulum. Sed bibendum nibh a neque accumsan, convallis convallis turpis lacinia. 
								Quisque aliquet arcu sit amet tortor efficitur volutpat. Maecenas interdum velit ultrices ligula ultricies tincidunt.</p>
 
								<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla. Phasellus 
								ornare eros luctus velit venenatis gravida. Cras imperdiet vulputate erat, sed bibendum neque rutrum in. Aliquam convallis 
								pulvinar velit non vestibulum. Phasellus placerat efficitur neque, in rutrum metus rhoncus non.</p>
								
							</div><!-- .article-body -->
						</div><!-- .main-body -->
						
						<aside class="sidebar">
							
							<?php include('inc/i-contact-box.php'); ?>
							
							<?php include('inc/i-book-callout.php'); ?>
							
						</aside><!-- .sidebar -->
						
					</div><!-- .sw -->
					
				</article>
				
				<!-- news results blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
						
							<div class="controls">
								<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
								<button class="control sprite-after abs arr-sm-right-white next">Next</button>
							</div><!-- .controls -->
						
							<form action="/" method="get" class="single-form search-form">
								<fieldset>
									<input type="text" name="s" placeholder="Search resources...">
									<input type="hidden" name="post_type" value="news">
									<button type="submit" class="sprite-after abs search-hover" title="Search Resources">Search Resources</button>
								</fieldset>
							</form><!-- .single-form.search-form -->
							
							<div class="selector">
								<select name="filter-sort-by" id="filter-sort-by">
									<option value="">Category</option>
									<!-- Use data-tag="" to provide a shorter string (to display in the selector after the user has selected it) for options that have long names -->
									<option value="service" selected data-tag="Service">Service</option>
									<option value="resource">Resource</option>
								</select>
								<span class="value">Category</span>
							</div><!-- .selector -->
						
							<div class="count">
								<span class="num">10</span> Resources Found
							</div><!-- .count -->
							
						</div><!-- .action-bar -->
					
						<div class="swiper-wrapper collapse-750">
							<div class="swipe">
								<div class="swipe-wrap">
									<div>										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
												
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
									</div>
									
									<div>										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
												
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
									</div>
									
									<div>										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-meta" href="#">
															<div class="content">
																<span class="title">Resource One</span>
																<span class="subtitle">Lorem Ipsum dolar sit amet</span>
																<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla.</p>
																<span class="button green">Download</span>
															</div><!-- .content -->
															<div class="meta">
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
												
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
									</div>
								
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>