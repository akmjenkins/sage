<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Our Services</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
			
				<div class="header">
					<div class="sw">
						<h1>Our Services</h1>
						<span class="subtitle">Aenean euismod bibendum laoreet proin.</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body">
						<div class="article-body">
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo 
							commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla 
							luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .sw -->
				
				<!-- overview blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="grid-wrap">
							<div class="grid fill eqh collapse-no-flex">
							
								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img" href="#">
											<div class="bubble-wrap">
												<span class="bubble sprite-after abs person">Take Care Of Yourself</span>
											</div><!-- .bubble-wrap -->
											<div class="content">
												<span class="title">Take Care Of Yourself</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img" href="#">
											<div class="bubble-wrap">
												<span class="bubble sprite-after abs people">Your Relationships</span>
											</div><!-- .bubble-wrap -->
											<div class="content">
												<span class="title">Your Relationships</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img" href="#">
											<div class="bubble-wrap">
												<span class="bubble sprite-after abs child">Your Child/Teen</span>
											</div><!-- .bubble-wrap -->
											<div class="content">
												<span class="title">Your Child/Teen</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img" href="#">
											<div class="bubble-wrap">
												<span class="bubble sprite-after abs heart">Physical Health</span>
											</div><!-- .bubble-wrap -->
											<div class="content">
												<span class="title">Physical Health</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img" href="#">
											<div class="bubble-wrap">
												<span class="bubble sprite-after abs briefcase">Company</span>
											</div><!-- .bubble-wrap -->
											<div class="content">
												<span class="title">Company</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->
								
							</div><!-- .grid.eqh -->
						</div><!-- .grid-wrap -->
					
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
				
			
			</div><!-- .body -->
			
<?php include('inc/i-footer.php'); ?>