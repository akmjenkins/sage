<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Our Services</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
			
				<div class="header">
					<div class="sw">
						<h1>Our Services</h1>
						<span class="subtitle">Aenean euismod bibendum laoreet proin.</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body">
						<div class="article-body">
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo 
							commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla 
							luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .sw -->
				
				<!-- overview blocks -->
				<div class="on-white blocks">
					<div class="sw">
						<?php include('inc/i-grid-view.php'); ?>
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
			</div><!-- .body -->
			
<?php include('inc/i-footer.php'); ?>