<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Contact Us</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
				<div class="header">
					<div class="sw">
						<h1>Contact Us</h1>
						<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body with-sidebar">
						<div class="article-body">				
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. 
								Donec eget eleifend justo. Nullam vel dui elit. Nam molestie vestibulum sollicitudin.
							</p>
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						<?php include('inc/i-contact-box.php'); ?>
					</aside><!-- .sidebar -->
				</div><!-- .sw.cf -->

				<section class="contact-section">
					<div class="sw">
						<div class="grid-wrap">
							<div class="grid eqh contact-grid collapse-850">
								<div class="col-2 col">
									<div>
										<h2>Contact Form</h2>
										
										<form action="/" method="post" class="body-form">
											<fieldset>
												<div class="grid pad5 collapse-450">
													<div class="col-2 col">
														<input type="text" name="fname" placeholder="First Name">		
													</div>
													<div class="col-2 col">
														<input type="text" name="lname" placeholder="Last Name">
													</div>
													<div class="col-1 col">
														<input type="email" name="email" placeholder="Email Address">		
													</div>
													<div class="col-1 col">
														<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>		
													</div>
												</div>
												<button class="button green" type="submit">Submit</button>
											</fieldset>
										</form><!-- .body-form -->
									</div>
								</div>
								<div class="col-2 col">
									<div class="locations">
								
										<h2>Location</h2>
										
										<div class="buttons">
											<button class="button green">Location One</button>
											<button class="button green">Location Two</button>
											<button class="button green">Location Three</button>
										</div><!-- .buttons -->
										
										<div class="swiper-wrapper">
											<div class="swipe">
												<div class="swipe-wrap">
													<div>
														
														<div class="address-block">
															<address>
																70 Rue King Street <br />
																Moncton NB E1C 4M6
															</address>
															
															<div class="phones">
																<span> P 1 506 857 3258</span>
																<span>TF 1 800 390 3258</span>
															</div><!-- .phones -->
														</div><!-- .address-block -->
														
														<div class="gmap">
															<div class="map" data-center="46.0923158,-64.7709474" data-zoom="15" data-markers='[{"title":"70 Rue King Street","position":"46.0923158,-64.7709474"}]'></div>
														</div>
														
													</div>
													<div>
														
														<div class="address-block">
															<address>
																874 Topsail Rd <br />
																Mt Pearl, NL A1N 3J9
															</address>
															
															<div class="phones">
																<span> P 1 709 754 0555</span>
															</div><!-- .phones -->
														</div><!-- .address-block -->
														
														<div class="gmap">
															<div class="map" data-center="47.524755,-52.793752" data-zoom="15" data-markers='[{"title":"JAC","position":"47.524755,-52.793752"}]'></div>
														</div><!-- .gmap -->
														
													</div>
													<div>
														
														<div class="address-block">
															<address>
																70 Rue King Street <br />
																Moncton NB E1C 4M6
															</address>
															
															<div class="phones">
																<span> P 1 506 857 3258</span>
																<span>TF 1 800 390 3258</span>
															</div><!-- .phones -->
														</div><!-- .address-block -->
														
														<div class="gmap">
															<div class="map" data-center="46.0923158,-64.7709474" data-zoom="15" data-markers='[{"title":"70 Rue King Street","position":"46.0923158,-64.7709474"}]'></div>
														</div>
														
													</div>
												</div><!-- .swipe-wrap -->
											</div><!-- .swipe -->
										</div><!-- .swiper-wrapper -->
									
									</div><!-- .locations -->
									
								</div>
							</div><!-- .grid.eqh -->
						</div><!-- .grid-wrap -->
					</div><!-- .sw -->
				</section><!-- .contact-section -->
				
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>