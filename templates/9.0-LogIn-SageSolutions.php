<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Contact Us</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
				<div class="header">
					<div class="sw">
						<h1>Contact Us</h1>
						<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body with-sidebar">
						<div class="article-body">				
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. 
								Donec eget eleifend justo. Nullam vel dui elit. Nam molestie vestibulum sollicitudin.
							</p>
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						<?php include('inc/i-contact-box.php'); ?>
					</aside><!-- .sidebar -->
				</div><!-- .sw.cf -->

				<section class="contact-section">
					<div class="sw">
						<div class="grid-wrap">
							<div class="grid eqh contact-grid collapse-800">
								<div class="col-2 col">
									<div>
										<h2>Existing Users</h2>
										
										<form action="/" method="post" class="body-form">
											<fieldset>
												<div class="grid pad5">
													<div class="col-1 col">
														<span class="block sprite-before abs field-wrap user">
															<input type="email" name="email" placeholder="E-mail Address">		
														</span>
													</div>
													<div class="col-1 col">
														<span class="block sprite-before abs field-wrap lock">
															<input type="password" name="password" placeholder="Password">
														</span>
													</div>
												</div>
												<button class="button green" type="submit">Log In</button>
												
												<div class="forgot article-body">
													Forgot your password? <br />
													Click <a href="#">here</a> to create a new one
												</div><!-- .forgot -->
												
											</fieldset>
										</form><!-- .body-form -->
									</div>
								</div><!-- .col -->
								<div class="col-2 col">
								
									<div>
									
										<h2>Become a Service Provider</h2>
									
										<form action="/" method="post" class="body-form full">
											<fieldset>
												
												<div class="grid pad5 collapse-450">
													<div class="col-2 col">
														<input type="text" name="fname" placeholder="First Name">		
													</div>
													<div class="col-2 col">
														<input type="text" name="lname" placeholder="Last Name">
													</div>
													<div class="col-2 col">
														<input type="email" name="email" placeholder="Email Address">
													</div>
													<div class="col-2 col">
														<input type="tel" pattern="\d+" name="phone" placeholder="Phone">
													</div>
													<div class="col-1 col">
														<select name="specialty">
															<option value="">Speciality</option>
															<option value="">Speciality 1</option>
															<option value="">Speciality 2</option>
															<option value="">Speciality 3</option>
														</select>
													</div>
													<div class="col-1 col">
														<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>				
													</div>
												</div><!-- .grid -->
												
												<button class="button green" type="submit">Register</button>
												
											</fieldset>
										</form><!-- .body-form -->
									</div>
									
								</div><!-- .col -->
							</div><!-- .grid.eqh -->
						</div><!-- .grid-wrap -->
					</div><!-- .sw -->
				</section><!-- .contact-section -->
				
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>