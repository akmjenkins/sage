				<section class="how-can-we-help">
					<div class="sw">
						<span class="h1-style">How Can We Help You?</span>
						<span class="section-subtitle">Aenean euismod bibendum laoreet proin gravida dolor sit amet lacus accumsan et viverra justo commodo. </span>
						<button class="toggle button green sprite-after abs arr-sm-down-white">Show Us How</button>
					</div><!-- .sw -->
					
					<div class="how-can-we-help-body">
						<div class="sw">
							<div class="tabs-wrap">
								<div class="tabs">
									<div class="tab">
										<span class="tab-title">Quick Help</span>
										<span class="tab-sub">Cras volutpat ultricies maurisplacerat.</span>
									</div>
									<div class="tab">
										<span class="tab-title">Ask A Question</span>
										<span class="tab-sub">Cras volutpat ultricies maurisplacerat.</span>							
									</div>
									<div class="tab">
										<span class="tab-title">Take Care Of</span>
										<span class="tab-sub">Cras volutpat ultricies maurisplacerat.</span>							
									</div>
								</div><!-- .tabs -->
							</div><!-- .tabs-wrap -->
						</div><!-- .sw -->
						
						<div class="swiper-wrapper">
							<div class="swipe tab-swiper">
								<div class="swipe-wrap">
									<div>
										
										<div class="sw">
											<div class="content">
											
												<span class="tab-title">Quick Help</span>
											
												<p>Aenean euismod bibendum laoreet proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
												
												<div class="buttons">
												
													<a href="#" class="button">
														<span class="sprite-after abs phone">Call Us</span>
														<span class="text">Call Us</span>
													</a><!-- .button -->
													
													<a href="#" class="button">
														<span class="sprite-after abs chat-md">Chat Now</span>
														<span class="text">Chat Now</span>
													</a><!-- .button -->
													
													<a href="#" class="button">
														<span class="sprite-after abs email">Email Us</span>
														<span class="text">Email Us</span>
													</a><!-- .button -->
													
												</div><!-- .buttons -->
												
											</div><!-- .content -->
										</div><!-- .sw -->
										
									</div>
									<div class="ask-a-question">
										
										<div class="sw">
											<div class="content">
											
												<span class="tab-title">Ask A Question</span>
											
												<p>Aenean euismod bibendum laoreet proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
												
												<!-- 
													the ACTION attribute must point to a URL that produces 
													the HTML output found in i-ask-question-responses.php
												-->
												<form action="inc/i-ask-question-responses.php" method="get" class="single-form ask-a-question-form">
													<fieldset>
														<input type="text" name="question" placeholder="Type your question here...">
														<button class="sprite-after abs question-mark" title="Ask">Ask</button>
													</fieldset>
												</form><!-- .single-form -->
											</div><!-- .content -->										
										</div><!-- .sw -->
										
									</div>
									<div>
										
										<div class="sw">
											<div class="content">
											
												<span class="tab-title">Take Care Of</span>
											
												<p>Aenean euismod bibendum laoreet proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</p>
											
												<div class="buttons">
												
													<button class="button filter active" data-slug="yourself" data-sprite="person">
														<span class="sprite-after abs person">Yourself</span>
														<span class="text">Yourself</span>
													</button><!-- .button -->
													
													<button class="button filter active" data-slug="relationships" data-sprite="people">
														<span class="sprite-after abs people">Relationships</span>
														<span class="text">Relationships</span>
													</button><!-- .button -->
													
													<button class="button filter active" data-slug="child-teen" data-sprite="child">
														<span class="sprite-after abs child">Child/Teen</span>
														<span class="text">Child/Teen</span>
													</button><!-- .button -->
													
													<button class="button filter active" data-slug="physical-health" data-sprite="heart">
														<span class="sprite-after abs heart">Physical Health</span>
														<span class="text">Physical Health</span>
													</button><!-- .button -->
													
													<button class="button filter active" data-slug="company" data-sprite="briefcase">
														<span class="sprite-after abs briefcase">Company</span>
														<span class="text">Company</span>
													</button><!-- .button -->
													
												</div><!-- .buttons -->
											
											</div><!-- .content -->
										</div><!-- .sw -->
										
									</div>
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
					</div><!-- .how-can-we-help-body -->
					
					<div class="how-can-we-help-content">
					
						<section class="how-can-we-help-services hidden">
							<?php include('i-services.php'); ?>
						</section><!-- .how-can-we-help-services -->
						
						<section class="how-can-we-help-results hidden sw">
							
							<div class="answers">
								<!-- dynamically load responses here -->	
							</div><!-- .answers -->
							
							<div class="error i-hidden">
								<span class="h4-style">Error!</span>
								<p>We weren't able to retrieve any answers to your question. Please try again.</p>
							</div><!-- .error -->
							
						</section><!-- .how-can-we-help-results -->
					
					</div><!-- .how-we-can-help-content -->
					
				</section><!-- .how-can-we-help -->